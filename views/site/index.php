<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="row text-center">
        <div class="jumbotron col-lg-8 col-lg-offset-2">
            <h2>Погода в Україні</h2>
            <form id="search-main-form" class="form-group form-search">
                <input type="text" placeholder="Enter City" class="form-control city-search float-left">
                <input type="submit" value="Прогноз" class="btn btn-success d-block btn-search"
                       data-toggle="tooltip" data-placement="top" title="Оберіть місто зі списку">

                <span class="last-pick d-none">
                <label for="city-last">Останні міста</label>
                <select id="city-last" class="select-gray">
                </select>
                    </span>

                <div class="search-links "></div>
            </form>


        </div>
    </div>
        <div class="row">
        <div class="forecast-block col-12 text-center"></div>


    </div>
</div>