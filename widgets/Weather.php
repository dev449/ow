<?php


namespace app\widgets;


use Yii;
use yii\base\Widget;
use app\models\Cities;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\db\Query;
use yii\web\View;

class weather extends Widget
{
    public $file;
    public $action;
    public $searchData;
    public $forecastData;


    public function init()
    {

        if (!isset($this->action)) return;
        parent::init();

    }

    public function run()
    {
        $this->file = Url::to('@webroot/ua-list-utf.csv');
        $this->{$this->action}();
    }

    public function generate()
    {
        $cityList = [];

        if (($handle = fopen($this->file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle)) !== FALSE) {
                $data = explode(';', $data[0]);
                $arr = [
                    'region',
                    'city',
                    'longitude',
                    'latitude',
                ];
                unset($data[1]);

                $map = array_combine($arr, $data);
                $cityList[] = $map;

            }
            fclose($handle);
        }

        if (!Cities::find()->one()) {
            foreach ($cityList as $city) {

                if (!empty($city['region']) && !empty($city['city'])) {


                    $cities = new Cities();
                    $cities->region = $city['region'];
                    $cities->city = $city['city'];
                    $cities->longitude = $city['longitude'];
                    $cities->latitude = $city['latitude'];

                    $cities->save();
                }
            }
        }
    }

    public function cleanse()
    {
        Cities::deleteAll();
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('ALTER TABLE ' . Cities::tableName() . ' AUTO_INCREMENT = 1');

        $command->query();
    }

    public function display()
    {
        $regionList = (new Query())
            ->select(['region', 'COUNT(region) AS count'])
            ->from(Cities::tableName())
            ->groupBy(['region'])
            ->orderBy(['count' => SORT_DESC])->all();
        $summary = Cities::find()->count();
        echo $this->render('weather/regions.php',['regionList' => $regionList, 'summary' => $summary]);
    }

    public function search()
    {
        $filterList = Cities::find()
            ->where('city like' . '\''.  HTML::encode($this->searchData) .'%\'')
            ->all();
        echo $this->render('weather/search.php',['filterList' => $filterList]);
    }

    public function forecast()
    {
        echo $this->render('weather/forecast.php',['forecastData' => $this->forecastData]);
    }


}