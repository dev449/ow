<?php

use app\models\Cities;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use yii\widgets\Pjax;

$dataProvider = new ActiveDataProvider([
    'query' => Cities::find()->where(['region' =>  $region])->orderBy('city'),
    'pagination' => [
        'pageSize' => 10,
        'params' => array_merge($_GET, ['region' => $region]),
    ],

]);
    Pjax::begin(['id' => uniqid(), 'enablePushState' => false]);
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_region',
        'layout' => "<span class='popup-region-title'>{$region} область</span>\n{summary}\n{items}\n{pager}"
    ]);
    Pjax::end();


