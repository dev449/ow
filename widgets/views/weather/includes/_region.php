<?php

use yii\helpers\HtmlPurifier;

?>
<btn class="btn list-group-item  text-left btn-city-forecast">
    <?= HtmlPurifier::process($model->city) ?>
</btn>
