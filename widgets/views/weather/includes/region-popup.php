<div class="modal fade" id="rozetka-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="regions-pjax"></div>
            </div>
            <div class="modal-footer">
                <div class="loader" style="display: none"></div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>

        </div>
    </div>
</div>