<h3 class="text-center"><?= $forecastData['city'] ?> (<?= $forecastData['region'] ?> Область)</h3>
<div class="row"></div>
<?php foreach ($forecastData['response']['daily'] as $forecast) : ?>
    <div class="<?= (array_search($forecast, $forecastData['response']['daily']) === 0) ?
        '' : '' ?> forecast-single col-auto">
       <span class="forecast-date"><?= date('d.m.y - ', $forecast['dt']) ?>
           <?= Yii::t('date', date('l', $forecast['dt']) ); ?></span>
        <div class="forecast-head text-center">
            <img src="https://openweathermap.org/img/wn/<?= $forecast['weather'][0]['icon'] . '.png' ?>">
        </div>
        <div class="forecast-body">
            <p>Ранок: <b><?= $forecast['temp']['morn'] ?>°C</b></p>
            <p>День: <b><?= $forecast['temp']['day'] ?>°C</b></p>
            <p>Вечір: <b><?= $forecast['temp']['eve'] ?>°C</b></p>
            <p>Ніч: <b><?= $forecast['temp']['night'] ?>°C</b></p>
            <span class="separator"></span>
            <p>Вологість: <b><?= $forecast['humidity'] ?>%</b></p>
            <p>Тиск: <b><?= $forecast['pressure'] * 0.75 ?> мм</b></p>
            <p>Вітер: <b><?= $forecast['wind_speed'] ?> м/с</b></p>

        </div>
    </div>

<?php endforeach; ?>
