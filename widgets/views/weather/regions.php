    <div class="nav nav-pills">
            <span class="list-group-item list-group-item-action active">
        Загалом
        <span class="float-right"><?= $summary ?></span>
    </span>
    <?php foreach ($regionList as $region) : ?>
        <btn class="list-group-item btn text-left btn-region-overview"
             data-region="<?= $region['region'] ?>" data-toggle="modal" data-target="#rozetka-modal">
            <?= $region['region'] ?>
            <span class="badge badge-primary badge-pill float-right"><?= $region['count'] ?></span>
        </btn>

    <?php endforeach; ?>
</div>

<?= $this->render('includes/region-popup.php') ?>
