    <div class="nav nav-pills">
    <?php foreach ($filterList as $filter) : ?>
        <btn class="list-group-item  text-left btn-search-result"
             data-lon="<?= $filter['longitude'] ?>"   data-lat="<?= $filter['latitude'] ?>">
            <span class="region-span"><?= $filter['region'] ?></span>
            <span class="float-right city-span"><?= $filter['city'] ?></span>
        </btn>
    <?php endforeach; ?>
</div>
