$(document).ready(function () {

    if ($('#cities_info').length) updateCityList();
    updateRecentSearch();

    $('.btn').click(function () {
        $(this).blur();
    });

    $('.btn_city_db').click(function () {
        $('.btn_city_db').each(function () {
            $(this).prop('disabled', true);
            $('.loader').show();
        })
    });

});
var searchBox = $('.city-search');
var searchBtn = $('.btn-search');
var searchForm = $('.form-search');

/** Поиск последних запросов в localstorage */
function updateRecentSearch() {

    var html = '';
    var lastCitySelect = $('#city-last');

    for (var counter = 4; counter >= 1; counter--) {
        if (localStorage.getItem('last_city' + counter)) {
            var strData = 'data-city=' + getSavedCity(counter, 'city');
            strData += ' ' + 'data-region=' + getSavedCity(counter, 'region');
            strData += ' ' + 'data-lon=' + getSavedCity(counter, 'lon');
            strData += ' ' + 'data-lat=' + getSavedCity(counter, 'lat');
            html += '<option class="city-last-option" ' + strData + '  >' + getSavedCity(counter, 'city') + '</option>';

            $('.last-pick').show();
        }
    }
    html += '<option value="" disabled selected hidden>Місто</option>';
    lastCitySelect.addClass('select-gray');
    lastCitySelect.html(html);


    lastCitySelect.change(function (ev) {

        $(this).removeClass('select-gray');

        var lastPick = $(ev.target.selectedOptions[0]);

        var data = {
            city: lastPick.data('city'),
            region: lastPick.data('region'),
            lon: lastPick.data('lon'),
            lat: lastPick.data('lat')
        };
        updateSearchBox(data);
    });

    function getSavedCity(counter, attr) {
        return JSON.parse(localStorage.getItem('last_city' + counter))[attr];
    }
}

/** **/


/** Основные запросы к виждету */

function updateCityList() {
    getAjax('display', false, {'fillResponse': '#cities_info'})
}

$('.btn_generate').click(function () {
    setTimeout(function () {
        getAjax('generate', false, {'updateCityList': null})
    }, 1000);

});
$('.btn_cleanse').click(function () {
    setTimeout(function () {
        getAjax('cleanse', false, {'updateCityList': null})
    }, 1000);
});

$(document).on('click', '.btn-region-overview', function () {
    $('.regions-pjax').load('/ajax/regions-pjax', {region: $(this).data('region')});
});
/** **/


/** Логика фильтрации в поиске */
searchBox.bind("paste keyup", function () {
    $(this).data('city', '');
    searchBtn.removeClass('search-pulse');

    var value = $(this).val();

    if (value.length > 1) getAjax('search', value, {'fillResponse': '.search-links'});
    else $('.search-links').html('');
});
/** **/

/** Логика при нажатии на выпадающий город */
$(document).on('click', '.btn-search-result', function () {

    var city = $(this).find('span.city-span').text();
    var region = $(this).find('span.region-span').text();

    var data = {
        city: city,
        region: region,
        lon: $(this).data('lon'),
        lat: $(this).data('lat')
    };
    updateSearchBox(data);

});

/** **/

/** Установка значения поля поиска */
function updateSearchBox(data) {


    searchBox.val(data.city);
    searchBox.data('city', data.city);
    searchBox.data('region', data.region);
    searchBox.data('lon', data.lon);
    searchBox.data('lat', data.lat);
    searchForm.removeClass('has-error');
    searchBtn.addClass('search-pulse');

    $('[data-toggle="tooltip"]').tooltip('destroy');
    $('.search-links').html('');

}

/** **/


/** Логика при отправке формы */
$('#search-main-form').on('submit', function (ev) {
    ev.preventDefault();
    if (!searchBox.data('city')) {
        searchForm.addClass('has-error');
        $('[data-toggle="tooltip"]').tooltip('show');

    } else {
        $('#search-main-form input').each(function () {
            $(this).attr('disabled', 'disabled');
        });
        searchBtn.removeClass('search-pulse');

        var data = {
            'city': searchBox.data('city'),
            'region': searchBox.data('region'),
            'lon': searchBox.data('lon'),
            'lat': searchBox.data('lat')
        };

        getForecast(data);


        // Обновление списка последних запросов
        for (var x = 1; x <= 4; x++) {

            if (JSON.stringify(data) === (localStorage.getItem('last_city' + x))) break;

            if (!localStorage.getItem('last_city' + x)) {
                localStorage.setItem('last_city' + x, JSON.stringify(data));
                break;
            } else if (x === 4) {
                localStorage.setItem('last_city1', localStorage.getItem('last_city2'));
                localStorage.setItem('last_city2', localStorage.getItem('last_city3'));
                localStorage.setItem('last_city3', localStorage.getItem('last_city4'));
                localStorage.setItem('last_city4', JSON.stringify(data));
            }
        }
        updateRecentSearch();
    }

});

/** **/

/** Обращение к API с подальшей обработкой виджетом */
function getForecast(data) {
    var apiSettings = {
        "async": true,
        "crossDomain": true,
        "url": 'https://api.openweathermap.org/data/2.5/onecall?lon=' + data.lon + '&lat=' + data.lat +
            '&exclude=current,minutely,hourly' + '&appid=6ca1797504971831963fc95689e9bd32&units=metric',
        "method": "POST"
    };

    $.ajax(apiSettings).done(function (response) {
        $('#search-main-form input').each(function () {
            $(this).attr('disabled', false);
        });
        var fullData = data;
        fullData.response = response;
        getAjax('forecast', fullData, {'fillResponse': '.forecast-block'});
    });
}
/** **/


/** Функция обработки ajax запросов в виджет */
function getAjax(action, data, callback) {
    var post = {'action': action, 'data': data};
    var settings = {
        "async": true,
        'url': '/ajax/index',
        "method": "POST",
        'data': post

    };

    $.ajax(settings).done(function (response) {

        if (callback) {
            $.each(callback, function (key, value) {
                try {
                    eval(key + '(\'' + value + '\')');
                } catch (e) {
                    console.log(e);
                }
            })
        }

        $('btn').each(function () {
            $(this).prop('disabled', false);
        });
        $('.loader').hide();

        function fillResponse(elem) {
            $(elem).html(response);
        }
    });
}
/** **/
