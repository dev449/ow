<?php


namespace app\controllers;

use app\widgets\Weather;
use yii\web\Controller;


class AjaxController extends Controller
{
    public $action;

    public function actionIndex()
    {
        if(isset($_POST['action']) && method_exists(get_class($this), $_POST['action'])){
            $this->action = $_POST['action'];
            return $this->{$this->action}();
        }
    }

    public function generate()
    {
        return Weather::widget(['action' => 'generate']);
    }

    public function cleanse()
    {
        return Weather::widget(['action' => 'cleanse']);
    }

    public function display()
    {
        return Weather::widget(['action' => 'display']);
    }

    public function search()
    {
        return Weather::widget(['action' => 'search', 'searchData' => $_POST['data']]);
    }

    public function forecast()
    {
        return Weather::widget(['action' => 'forecast', 'forecastData' => $_POST['data']]);
    }


    public function actionRegionsPjax()
    {

        $region = false;

        if (isset($_POST['region'])) $region = $_POST['region'];
        if (isset($_GET['region'])) $region = $_GET['region'];


        return $this->renderAjax('../../widgets/views/weather/includes/region-pjax.php', ['region' => $region]);
    }
}